from typing import List, Optional, Union, Literal, Tuple

from pydantic import BaseModel, EmailStr, Field
from schema import PersonalService, Daemon, Database, DevService

DOMAIN_REGEX = r"^[a-zA-Z0-9][a-zA-Z0-9-]{1,61}[a-zA-Z0-9](?:\.[a-zA-Z]{2,})+$"
KEY_REGEX = r"^[0-9a-fA-F]{32}"
PASSWORD_REGEX = r"^.*(?=.{14,})(?=.*[a-zA-Z])(?=.*\d)(?=.*[!@#$%^&*]).*$"  # nosec B105
ServicePromptResponse = Tuple[List[PersonalService], List[Database], List[Daemon]]


class Question(BaseModel):
    type: Literal["confirm", "text", "password", "checkbox"]
    name: str
    message: str
    when: Optional[bool]


class Name(BaseModel):
    name: str
    checked: Optional[bool]
    disabled: Optional[str]


class Separator(BaseModel):
    separator: str


Choices = List[Union[Name, Separator]]


class CheckboxQuestion(Question):
    qmark: str
    choices: Choices


class VariablesQuestion(Question):
    qmark: str


class Variables(BaseModel):
    email: EmailStr
    domain: str = Field(..., regex=DOMAIN_REGEX)
    namecheap_username: str
    namecheap_api_key: str
    master_password: str


class Spec(BaseModel):
    variables: Variables
    services: List[Union[PersonalService, DevService]]
    databases: List[Database]
    daemons: List[Daemon]


ServiceType = Literal["personal", "dev"]
