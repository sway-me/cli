import click


def intro(title: str) -> None:
    click.echo(
        "\n==================== Welcome to"
        + click.style(" S W A Y ", bold=True, fg="bright_cyan")
        + f"{title}  ====================\n"
    )


def error_message(msg: str) -> None:
    click.echo(f"\n❌  {click.style(msg, bold=True, fg='bright_red')}\n")


def no_prob() -> None:
    click.echo(
        """
    No prob. Please visit:
        https://docs.sway.cx/personal-services/getting-started/
    for more info
    """
    )
