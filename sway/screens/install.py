import sys
from typing import List, cast, Any

import questionary

from components import intro, error_message, no_prob
from models import Question, Variables, CheckboxQuestion, ServiceType, VariablesQuestion
from utils import (
    get_questions,
    convert_separators,
    get_deps_and_opts,
    filter_options,
    answers2types,
    inputs_prompt,
    check_against_system,
    create_spec,
    is_connected,
)


def process_initial_question(proceed: bool = True, **kwargs: Any) -> bool:
    if not proceed:
        return False
    if not is_connected():
        error_message("Must be connected to internet")
        sys.exit()
    initial_question = get_questions("initial.yml", Question)
    initial_answer = questionary.prompt(initial_question.dict(), **kwargs)
    if "continue" not in initial_answer:
        no_prob()
        sys.exit()
    return bool(initial_answer["continue"])


def process_variables(proceed: bool, **kwargs: Any) -> Variables:
    if not proceed:
        sys.exit()
    variable_questions = get_questions("variables.yml", VariablesQuestion)
    variables_answers = inputs_prompt(
        [q.dict() for q in variable_questions], Variables, **kwargs
    )
    if not variables_answers:
        sys.exit()
    return variables_answers


def process_services(
    proceed: bool,
    service_type: ServiceType,
    **kwargs: Any,
) -> List[str]:
    if not proceed:
        sys.exit()
    service_questions = get_questions(f"{service_type}-services.yml", CheckboxQuestion)
    updated_choices = convert_separators(service_questions.choices)
    updated_choices = check_against_system(updated_choices)
    service_questions.choices = updated_choices
    style = questionary.Style([("selected", "noreverse")])
    answers = questionary.prompt(service_questions.dict(), style=style, **kwargs)
    if not answers:
        sys.exit()
    if answers == {"services": []}:
        error_message("You must select at least one service")
        sys.exit()
    return cast(List[str], answers["services"])


def process_options(proceed: bool, opts: List[str], **kwargs: Any) -> List[str]:
    if not proceed:
        sys.exit()
    option_questions = get_questions("options.yml", CheckboxQuestion)
    filtered_opt_questions = filter_options(opts, option_questions)
    if not filtered_opt_questions.choices:
        # no options to select
        return []
    answers = questionary.prompt(filtered_opt_questions.dict(), **kwargs)
    return cast(List[str], answers["options"])


def process_cryptos(proceed: bool, opt_names: List[str], **kwargs: Any) -> List[str]:
    if not proceed:
        return []
    for opt in opt_names:
        if "CPUMiner" in opt:
            cryptos_question = get_questions("cryptos.yml", CheckboxQuestion)
            answers = questionary.prompt(cryptos_question.dict(), **kwargs)
            if "cryptos" not in answers:
                # no options to select
                return []
            return cast(List[str], answers["cryptos"])
    return []


def main(service_type: ServiceType) -> None:
    intro(
        f"{service_type.title()} Services { '🔒' if service_type == 'personal' else '☁️'}"
    )
    proceed = process_initial_question()
    variables = process_variables(proceed)
    service_answers = process_services(any(variables.dict().values()), service_type)
    services = answers2types(service_answers)
    dependency_answers, option_answers = get_deps_and_opts(service_answers)
    dependencies = answers2types(dependency_answers)
    selection_answers = process_options(bool(option_answers), option_answers)
    selections = answers2types(selection_answers)
    cryptos_answers = process_cryptos(bool(selection_answers), selection_answers)
    cryptos = answers2types(cryptos_answers)
    spec = create_spec(variables, services + dependencies + selections + cryptos)
    print(spec)
