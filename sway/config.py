import logging
import os
from typing import Literal, Dict, Optional, cast

from pydantic import BaseModel, HttpUrl


class Config(BaseModel):
    log_level: Optional[Literal["ERROR", "WARN", "INFO", "DEBUG"]] = None
    sentry_url: Optional[HttpUrl] = None
    log_file_path: str = "/dev/null"


Keys = Literal["local", "development", "beta", "production"]
ConfigMap = Dict[Keys, Config]

config_map: ConfigMap = {
    "local": Config(),
    "development": Config(),
    "beta": Config(
        log_level="INFO",
        log_file_path="/var/log/sway.log",
        # TODO: get a sentry url with an account from this project
        sentry_url=cast(
            HttpUrl,
            "https://4ec782ec2e654c8ea3b6994f01c54882@o409017.ingest.sentry.io/5948203",
        ),
    ),
    "production": Config(
        log_level="INFO",
        log_file_path="/var/log/sway.log",
        sentry_url=cast(
            HttpUrl,
            "https://4ec782ec2e654c8ea3b6994f01c54882@o409017.ingest.sentry.io/5948203",
        ),
    ),
}


config = config_map[cast(Keys, os.getenv("PYTHON_ENV", "local"))]


def setup_logger(cfg: Config) -> None:
    level = cfg.log_level
    logging.basicConfig(
        filename=cfg.log_file_path,
        encoding="utf-8",
        format="%(levelname)s: %(message)s",
        level=level,
    )
