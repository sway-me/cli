import click
import sentry_sdk

import screens
from config import config, setup_logger
from utils import run


@click.version_option()
@click.group()
def cli() -> None:
    sentry_sdk.init(config.sentry_url)
    setup_logger(config)


@cli.group()
def dev() -> None:
    pass


@dev.command("install")
def dev_install() -> None:
    screens.install.main("dev")


@dev.command("uninstall")
def dev_uninstall() -> None:
    click.echo("Initializing dev uninstall!")


@cli.group()
def personal() -> None:
    pass


@personal.command("install")
def personal_install() -> None:
    screens.install.main("personal")


@personal.command("uninstall")
def personal_uninstall() -> None:
    click.echo("Initializing personal uninstall!")


@personal.group()
def home_assistant() -> None:
    pass


@home_assistant.command("install")
def home_assistant_install() -> None:
    click.echo("Initializing homeassistant install!")


@home_assistant.command("uninstall")
def home_assistant_uninstall() -> None:
    click.echo("Initializing homeassistant uninstall!")


@cli.command()
@click.argument("plugin_file")
def plugin(plugin_file: str) -> None:
    click.echo(plugin_file)


@cli.command()
def update() -> None:
    click.echo("updates")


@cli.group()
def autocomplete() -> None:
    pass


@autocomplete.command()
def zsh() -> None:
    run("_SWAY_COMPLETE=zsh_source sway > ~/.sway-complete.zsh")
    run("echo '. ~/.sway-complete.zsh'  >> ~/.zshrc")
    click.echo("zsh autocomplete installed. please restart zsh")


@autocomplete.command()
def bash() -> None:
    run("_SWAY_COMPLETE=bash_source sway > ~/.sway-complete.bash")
    run("echo '. ~/.sway-complete.bash'  >> ~/.bashrc")
    click.echo("bash autocomplete installed. please restart bash")
