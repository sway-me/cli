# generated by datamodel-codegen:
#   filename:  schema.json
#   timestamp: 2022-06-26T02:21:01+00:00

from __future__ import annotations

from enum import Enum
from typing import List, Optional, Union, Literal

from pydantic import AnyUrl, BaseModel, Field


class Subcategory(Enum):
    Databases_And_Storage = "Databases And Storage"
    App_Services = "App Services"
    BaaS = "BaaS"
    Team_Services = "Team Services"
    Messaging = "Messaging"
    Metrics = "Metrics"
    Media = "Media"
    Groupware = "Groupware"
    Social_Media = "Social Media"
    Miscellaneous = "Miscellaneous"


class Category(Enum):
    personal_services = "personal-services"
    dev_services = "dev-services"
    android_clients = "android-clients"
    web_clients = "web-clients"
    operating_systems = "operating-systems"
    terminal_clients = "terminal-clients"
    daemons = "daemons"
    databases = "databases"


class Repo(BaseModel):
    url: AnyUrl
    title: str
    rating: Optional[int] = None


class AndroidClients(Enum):
    antennapod = "antennapod"
    bitpay = "bitpay"
    fluent_reader = "fluent-reader"
    davx5 = "davx5"
    geometric_weather = "geometric-weather"
    k9_mail = "k9-mail"
    librera = "librera"
    microg = "microg"
    orgzly = "orgzly"
    simple_calendar = "simple-calendar"
    simple_contacts = "simple-contacts"
    simple_gallery = "simple-gallery"
    termux = "termux"
    tusky = "tusky"
    weechat_android = "weechat-android"


class Daemon(Enum):
    docker = "docker"
    restic = "restic"
    bitcoind = "bitcoind"
    openethereum = "openethereum"
    cpuminer = "cpuminer"


class Database(Enum):
    mariadb = "mariadb"
    mongodb = "mongodb"
    postgres = "postgres"
    redis = "redis"


class DevService(Enum):
    appwrite = "appwrite"
    bitwarden = "bitwarden"
    community = "community"
    dokku = "dokku"
    emqx = "emqx"
    garie = "garie"
    gitlab = "gitlab"
    gorush = "gorush"
    grafana = "grafana"
    hasura = "hasura"
    kafka = "kafka"
    keycloak = "keycloak"
    loki = "loki"
    matrix = "matrix"
    minio = "minio"
    postal = "postal"
    sentry = "sentry"
    textbelt = "textbelt"
    traefik = "traefik"


class OperatingSystems(Enum):
    arch_linux = "arch-linux"
    asteroidos = "asteroidos"
    debian = "debian"
    lineageos = "lineageos"


class PersonalService(Enum):
    audiobookshelf = "audiobookshelf"
    bitcore = "bitcore"
    calibre_web = "calibre-web"
    collabora = "collabora"
    freshrss = "freshrss"
    funkwhale = "funkwhale"
    home_assistant = "home-assistant"
    hydroxide = "hydroxide"
    jellyfin = "jellyfin"
    joplin = "joplin"
    mastodon = "mastodon"
    mopidy = "mopidy"
    mycroft = "mycroft"
    nextcloud = "nextcloud"
    paperless = "paperless"
    librephotos = "librephotos"
    snapcast = "snapcast"
    syncthing = "syncthing"
    weechat = "weechat"
    wger = "wger"


class TerminalClients(Enum):
    castero = "castero"
    cava = "cava"
    emacs = "emacs"
    epr = "epr"
    feh = "feh"
    khal = "khal"
    khard = "khard"
    mpv = "mpv"
    ncmpcpp = "ncmpcpp"
    neomutt = "neomutt"
    newsboat = "newsboat"
    ranger = "ranger"


class WebClients(Enum):
    glowing_bear = "glowing-bear"


class Resource(BaseModel):
    subcategory: Optional[Subcategory] = None
    options: Optional[List[Union[WebClients, PersonalService, Daemon]]] = None
    dependencies: Optional[List[Union[PersonalService, Database, Daemon]]] = None
    name: Union[
        PersonalService,
        DevService,
        AndroidClients,
        TerminalClients,
        WebClients,
        OperatingSystems,
        Daemon,
        Database,
    ]
    description: str
    category: Category
    repos: List[Repo] = Field(..., min_items=1, unique_items=True)
