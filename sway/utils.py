import socket
import re
import subprocess  # nosec B404
from typing import (
    Dict,
    TypeVar,
    List,
    Tuple,
    Optional,
    Any,
    Iterable,
    Mapping,
    Type,
    Union,
)

import email_validator
import psutil
import questionary
import yaml
from pydantic import BaseModel

from components import error_message
from models import (
    DOMAIN_REGEX,
    CheckboxQuestion,
    Separator,
    Choices,
    KEY_REGEX,
    PASSWORD_REGEX,
    Variables,
    Spec,
)
from schema import Daemon, PersonalService, Database, Resource


def is_connected() -> bool:
    try:
        socket.create_connection(("1.1.1.1", 53))
        return True
    except OSError:
        pass
    return False


T = TypeVar("T")


class SystemInfo(BaseModel):
    cpus: int
    freq: float
    memory: int
    free_space: int


def get_system_info() -> SystemInfo:
    return SystemInfo(
        cpus=psutil.cpu_count(),
        freq=psutil.cpu_freq().max,
        memory=psutil.virtual_memory().total,
        free_space=psutil.disk_usage("/").free,
    )


def run(cmd: str) -> None:
    subprocess.run(cmd, shell=True, check=False)  # nosec B602


def get_questions(path: str, cls: Type[T]) -> Union[T, List[T]]:
    with open(f"data/questions/{path}", "rb") as file:
        data = yaml.safe_load(file)
        if isinstance(data, list):
            return [cls(**q) for q in data]
        return cls(**data)


def validate_email(val: str) -> bool:
    try:
        email_validator.validate_email(val)
        return True
    except email_validator.EmailNotValidError:
        error_message("value is not a valid email address")
        return False


def validate_domain(val: str) -> bool:
    match_result = re.match(DOMAIN_REGEX, val)
    if not bool(match_result):
        error_message("value is not a valid domain")
        return False
    return True


def validate_api_key(val: str) -> bool:
    match_result = re.match(KEY_REGEX, val)
    if not bool(match_result):
        error_message("value is not a valid api key")
        return False
    return True


def validate_password(val: str) -> bool:
    match_result = re.match(PASSWORD_REGEX, val)
    if not bool(match_result):
        error_message(
            "Value is not a valid password."
            + " Must be a min of 14 chars, 1 uppercase, 1 lowercase,"
            + " 1 number and any of these special chars: !@#$%^&*. eg: S0m3StrongP@$$w0rd "
        )
        return False
    return True


def check_against_system(choices: Choices) -> Choices:
    system_info = get_system_info()
    for choice in choices:
        if not isinstance(choice, questionary.Separator):
            if "bitcore" in choice.name.lower():
                if system_info.free_space < 250 * 1024**3:
                    choice.disabled = "Unable to select. 250GB free space required"
                    choice.checked = False
    return choices


def create_spec(variables: Variables, resources: List[Resource]) -> Spec:
    personal_services = [r for r in resources if isinstance(r, PersonalService)]
    daemons = [r for r in resources if isinstance(r, Daemon)]
    databases = [r for r in resources if isinstance(r, Database)]
    return Spec(
        variables=variables,
        services=personal_services,
        daemons=daemons,
        databases=databases,
    )


def validate(key: str, val: str) -> bool:
    if "email" in key:
        return validate_email(val)
    if "domain" in key:
        return validate_domain(val)
    if "api_key" in key:
        return validate_api_key(val)
    if "password" in key:
        return validate_password(val)
    if "" == val:
        error_message("value is required")
        return False
    return True


def inputs_prompt(
    questions: List[Iterable[Mapping[str, Any]]], cls: Type[T], **kwargs: Any
) -> Optional[T]:
    answers: Dict[str, str] = {}
    for question in questions:
        valid = False
        while not valid:
            res = questionary.prompt(question, **kwargs)
            if not res:
                return None
            key = list(res.keys())[0]
            val = list(res.values())[0]
            valid = validate(key, val)
            if res:
                answers = answers | {key: val}
    return cls(**answers)


def convert_separators(choices: Choices) -> List[object]:
    return [
        questionary.Separator(f"\n{c.separator}") if isinstance(c, Separator) else c
        for c in choices
    ]


def get_deps_and_opts(
    service_answers: List[str],
) -> Tuple[List[str], List[str]]:
    if not service_answers:
        return [], []
    deps = []
    opts = []
    for service in service_answers:
        if "Dependencies" in service or "Optional" in service:
            deps_opts = service.split("Dependencies:")[1].split(". Optional:")
            current_deps: List[str] = deps_opts[0].split()
            deps += current_deps
            if len(deps_opts) > 1:
                current_opts: List[str] = deps_opts[1].split()
                opts += current_opts
    return list(set(deps)), opts


def filter_options(opts: List, opt_questions: CheckboxQuestion) -> CheckboxQuestion:
    filtered = []
    for choice in opt_questions.choices:
        for option in opts:
            if option in choice.name.lower():
                filtered.append(choice)
    opt_questions.choices = filtered
    return opt_questions


def answers2types(answers: Optional[List[str]]) -> Any:
    types = []
    if not answers:
        return []
    strings = [answer.split(":")[0].lower() for answer in answers]
    for string in strings:
        if string in ("openethereum", "cpuminer", "bitcoind", "litecoind"):
            types.append(Daemon(string))
        elif string in ("postgres", "mariadb", "redis", "mongodb"):
            types.append(Database(string))
        else:
            types.append(PersonalService(string))
    return types
