import asyncio
from typing import List, cast

from aiohttp import ClientSession

from schema import Resource


async def get_data(session: ClientSession) -> List[Resource]:
    async with session.get("https://minio.sway.cx/data/data.json") as response:
        blobs = await response.json()
        return [Resource(**blob) for blob in blobs]


async def main() -> None:
    session = ClientSession()
    resources = await get_data(session)

    await session.close()


asyncio.run(main())
