from setuptools import setup

setup(
    name="sway",
    version="1.0.0b0",
    py_modules=["sway"],
    install_requires=[
        "Click",
    ],
    entry_points={
        "console_scripts": [
            "sway = sway.main:cli",
        ],
    },
)
