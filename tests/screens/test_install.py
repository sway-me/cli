# pylint: disable=unused-variable
from enum import Enum
from importlib.metadata import version
from typing import Callable, Any, List, Literal

from prompt_toolkit.input import create_pipe_input
from prompt_toolkit.output import DummyOutput
from click.testing import CliRunner

from models import ServiceType
from screens.install import (
    process_variables,
    process_services,
    process_options,
    process_cryptos,
    process_initial_question,
)
from sway.main import cli

click_run = CliRunner().invoke


class Press(Enum):
    DOWN = "\x1b[B"
    UP = "\x1b[A"
    LEFT = "\x1b[D"
    RIGHT = "\x1b[C"
    ESCAPE = "\x1b"
    CONTROLC = "\x03"
    CONTROLN = "\x0e"
    CONTROLP = "\x10"
    BACK = "\x7f"
    SPACE = " "
    TAB = "\x09"


# helpers
def questionary_run(
    mock_process: Callable,
    text: str,
) -> Any:
    options = ["Funkwhale", "Jellyfin"]
    with create_pipe_input() as inp:
        inp.send_text(text)
        return mock_process(True, input=inp, output=DummyOutput())


def questionary_run_services(
    mock_process: Callable,
    text: str,
    service_type: ServiceType,
) -> Any:
    with create_pipe_input() as inp:
        inp.send_text(text)
        return mock_process(True, service_type, input=inp, output=DummyOutput())


def questionary_run_options(
    mock_process: Callable,
    text: str,
    options: List[str],
) -> Any:
    with create_pipe_input() as inp:
        inp.send_text(text)
        return mock_process(True, options, input=inp, output=DummyOutput())


def describe_sway_options() -> None:
    def it_displays_correct_version() -> None:
        output = click_run(cli, "--version").output
        assert version("sway") in output


def format_answers(
    prompt_type: Literal["input", "checkbox"], answers: List[str]
) -> str:
    if prompt_type == "input":
        return "".join([f"{a}\r" for a in answers])
    return "".join([" \r"] * len(answers))


def describe_personal_services_screens() -> None:
    def it_responds_to_initial_prompt() -> None:
        response = questionary_run(process_initial_question, "y\r")
        assert response
        response = questionary_run(process_initial_question, "n\r")
        assert not response

    def it_responds_to_variables_prompt() -> None:
        answers = [
            "test@test.com",
            "test.com",
            "test_user",
            "8de0d5ca5d706a5ab419a183c62e8afb",
            "S0m3StrongP@$$w0rd",
        ]
        response = questionary_run(process_variables, format_answers("input", answers))
        for i, (_, val) in enumerate(response):
            assert val == answers[i]

    def it_responds_to_services_prompt_funkwhale_selection() -> None:
        expected = ["Funkwhale", "Jellyfin"]
        service_answers = questionary_run_services(
            process_services, format_answers("checkbox", expected), "personal"
        )
        for i, val in enumerate(service_answers):
            assert expected[i] in val

    def it_responds_to_options_prompt() -> None:
        options_available = ["collabora"]
        answers = questionary_run_options(process_options, "a\r", options_available)
        for idx, answer in enumerate(answers):
            assert options_available[idx] in answer.lower()

    def it_responds_to_cryptos_prompt() -> None:
        cryptos_available = ["openethereum"]
        answers = questionary_run_options(process_cryptos, "a\r", cryptos_available)
        for idx, answer in enumerate(answers):
            assert cryptos_available[idx] in answer.lower()


def describe_dev_services() -> None:
    def it_responds_to_initial_prompt() -> None:
        pass
