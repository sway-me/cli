# pylint: disable=unused-variable
from importlib.metadata import version
from subprocess import PIPE, STDOUT, Popen


# helpers


def run(cmd: str, user_input: str = "") -> str:
    with Popen(cmd.split(), stdout=PIPE, stdin=PIPE, stderr=STDOUT) as process:
        response = process.communicate(user_input.encode())
        return response[0].decode("utf-8")


def describe_sway_options() -> None:
    def it_displays_correct_version() -> None:
        output = run("sway --version")
        assert version("sway") in output


inputs = [
    "y",
    "test@test.com\r"
    + "test.com\r"
    + "some_user\r"
    + "8de0d5ca5d706a5ab419a183c62e8afb\r"
    + "S0m3StrongP@$$w0rd\r",
    "a\r",
    "a\r",
    # "a\r",
]


def describe_personal_services_screens() -> None:
    def it_receives_yes_to_initial_prompt() -> None:
        output = run("sway personal install", inputs[0])
        assert "Personal Services" in output
        assert "email used to manage" in output

    def it_advances_to_services_prompt() -> None:
        output = run("sway personal install", "".join(inputs[0:2]))
        assert "📺 Media" in output

    def it_advances_to_options_prompt() -> None:
        output = run("sway personal install", "".join(inputs[0:3]))
        assert "Optional Services" in output

    # def it_advances_to_cryptos_prompt() -> None:
    #     with patch("psutil.disk_usage") as mock_free:
    #         mock_free("/").free = 1 * 1024**3
    #         output = run("sway personal install", "".join(inputs[0:4]))
    #         assert "Cryptocurrencies to mine" in output
