# Sway CLI 
 Installs personal and dev services

![](https://img.shields.io/gitlab/pipeline-status/sway-me/cli?branch=development&logo=gitlab&style=for-the-badge)
![](https://img.shields.io/gitlab/coverage/sway-me/cli/development?logo=pytest&style=for-the-badge)

[//]: # (![]&#40;https://img.shields.io/aur/version/sway-cli?logo=arch-linux&style=for-the-badge&#41;)
[//]: # (![]&#40;https://img.shields.io/debian/v/sway-cli/unstable?color=dd1155&logo=debian&style=for-the-badge&#41;)

```shell
Usage: sway [OPTIONS] COMMAND [ARGS]...

Options:
  --version  Show the version and exit.
  --help     Show this message and exit.

Commands:
  autocomplete
  dev
  personal
  plugin
  update
```

